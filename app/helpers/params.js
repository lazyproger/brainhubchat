'use strict';

module.exports = {
    parseCookies : (data) => {
        let cookies = {};
    
        data && data.split(';').forEach(function( cookie ) {
            let parts = cookie.split('=');
            cookies[parts[0].trim()] = (parts[1] || '').trim();
        });
    
        return cookies;
    },    
    
    checkInteger : (value) => {
        let returnInt   = 0,
            originalInt = value;
        
        if(value) {
            value = parseInt(value);        
            if(!isNaN(value) && isFinite(value) && value == originalInt) {
                returnInt = value;    
             }
        }
        
        return returnInt;   
    }
};