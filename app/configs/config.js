'use strict';
const module_fs = require('fs');


module.exports = {
    server_port : 3000, 
    
    server_options : {
        key  : module_fs.readFileSync('certificates/private.key'),
        cert : module_fs.readFileSync('certificates/sportday_today.crt'),
        ca   : module_fs.readFileSync('certificates/sportday_today.ca-bundle')    
    },

	mysql : {
		db_host : 'localhost',
		db_user : 'isport',
		db_password : '6L0v9C1v',
		db_name : 'eburg',
		
	},
    
    memcached   : {
            hosts   : ['localhost:11211'],
            options : {
                failures  : 1,
                reconnect : 3000,
                retries   : 1,
                timeout   : 1000, 
                retry     : 1000 
            },
            keys    : {
                session : 'memc.sess.',
                room    : 'chat.room'
                
            }
    }
};