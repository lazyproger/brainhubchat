'use strict';

const module_config         =  require('./app/configs/config'),
      module_params         =  require('./app/helpers/params'),
      module_server         =  require('http'),
      module_ioSocket       =  require('socket.io').listen(3000),
      module_memcached      =  require('memcached'),
	  module_mysql			=  require('mysql'),
	  // module_server         =  require('https').createServer(module_config.server_options, (req, res) => {}).listen(module_config.server_port),
      module_phpUnserialize =  require('php-unserialize'),
      Memcached             = new module_memcached(module_config.memcached.hosts, module_config.memcached.options);


	  
Memcached.on('issue', (details) => {
    console.log(details);
})

//Connect DB
var mysql = module_mysql.createConnection({
	host     :	module_config.mysql.db_host,
	user     :	module_config.mysql.db_user,
	password :	module_config.mysql.db_password,
	database :	module_config.mysql.db_name
});
		
mysql.connect();


let history = {},
    cleanRoomMsg   = 'очистил комнату',
    addUserMsg     = 'вошел в чат',
    welcomeUserMsg = ', добро пожаловать', 
    sysUser        = {
            uid         : 0,    
            name        : 'Система',
            pic_square  : '/templates/default/chat/img/sys.png'            
    };

module_ioSocket.set('authorization', userSignIn);

function userSignIn (data, callback) {
    if(data && data.headers && data.headers.cookie && data._query && data._query.room) {
        let cookies = module_params.parseCookies(data.headers.cookie);
        
		console.log(data._query.room);
		// && module_params.checkInteger(data._query.room)
        if(cookies.sid && data._query.room) {
			
			console.log('step 1 ok');
			
            let sessionKey  =  module_config.memcached.keys.session + cookies.sid,
                chatRoomKey = module_config.memcached.keys.room + data._query.room;
            
            Memcached.getMulti([sessionKey, chatRoomKey], (mcachedError, mcachedData) => {
                let session = mcachedData[sessionKey] ? mcachedData[sessionKey] : false,
                    room    = mcachedData[chatRoomKey] ? JSON.parse(mcachedData[chatRoomKey]) : false;                    
                
                if(session) {
                    try{
                        session = module_phpUnserialize.unserializeSession(session);    
                    }
                    catch(e) {
                        session = false;    
                    }
                }
                

                if(session && session.userData && room) {
                    data.chatUser = session.userData; 
                    data.roomData = Object.assign(room, {id : data._query.room}); 
                    callback(null, true);
                } else {
					console.log('Доступ закрыт, недостаточно данных');
                    callback(new Error('Доступ закрыт'))    
                }            
            }); 
        } else {
			console.log('Доступ закрыт');
            callback(new Error('Доступ закрыт'));
        } 
    } else {
        callback(new Error('Доступ закрыт'));
    }    
} 

module_ioSocket.on('connection', (socket) => {
	
	console.log('- User connection');
	
	
	 // && module_params.checkInteger(socket.request.roomData.id)
    if(socket.request && socket.request.chatUser  && socket.request.roomData && socket.request.roomData.id) {
        let room     = socket.request.roomData.id,
            userData = socket.request.chatUser; 
        
        if(!history[room]) history[room] = [];
		
        socket.join(room);   
		//Когда юзер присоединился, сделать счетчик онлайна
        socket.emit('userSignIn', history[room] || [], {text : userData.name + welcomeUserMsg, user : sysUser, sys : 1});
        
        socket.broadcast.in(room).emit('message', {text : userData.name + ' ' + addUserMsg, user : sysUser, sys : 1});
        
        socket.on('message', (msg) => {
            if(!msg) return;
            
            history[room].push({text : msg, user : userData, sys : 0});
            
            if(history[room].length > 10) {
                history[room].splice(0, 1);
            }
			
			//Отправляем сообщение в базу
			mysql.query('INSERT INTO `chat_test` SET ?', {user_id : sysUser, text : msg, room : room}, function (error, results, fields) {
				if (error) throw error;
			});
            
            module_ioSocket.in(room).emit('message', {text : msg, user : userData, sys : 0});        
        });
        
        socket.on('cleanRoom', () => {
            if(userData.rights == 10) {
                history[room] = [];
                module_ioSocket.in(room).emit('cleanRoom', {text : userData.name + ' ' + cleanRoomMsg, user : sysUser, sys : 1}); 
            }
        });
        
        socket.on('disconnect', (error) => {
            socket.leave(room);
        });
    
    } else {
        socket.emit('errorChat', 'Access closed')    
    }
    
    // disconect leave error
});

console.log('vse ok');